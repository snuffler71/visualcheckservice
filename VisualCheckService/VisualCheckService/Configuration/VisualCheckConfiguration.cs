﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace VisualCheckService.Configuration
{
    public static class VisualCheckConfiguration
    {
        public static string EventLogSource = ConfigurationManager.AppSettings["EventLogSource"];
        public static string EventLog = ConfigurationManager.AppSettings["EventLog"];
        public static string Directory = ConfigurationManager.AppSettings["Directory"];
        public static string Pattern = ConfigurationManager.AppSettings["Pattern"];
        public static List<String> SupportedFileTypes = ConfigurationManager.AppSettings["SupportedFileTypes"].Split(',').ToList();
        public static string ServiceType = ConfigurationManager.AppSettings["ServiceType"];
        public static string Success = "Visual Check file cleanup successsful, {0} files deleted";
    }

    public enum CleanUpServiceType
    {
        File
    }
}
