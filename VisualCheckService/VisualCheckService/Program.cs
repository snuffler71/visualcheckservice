﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using VisualCheckService.Configuration;

namespace VisualCheckService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new VisualCheckService(CleanUpServiceType.File) 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
