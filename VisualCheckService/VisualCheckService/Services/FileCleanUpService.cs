﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using VisualCheckService.Interfaces;
using VisualCheckService.Configuration;
using System.Diagnostics;

namespace VisualCheckService.Services
{
    /// <summary>
    /// FileCleanUpService. This service will purge a directory of media files matching a specific pattern.
    /// </summary>
    public class FileCleanUpService: ICleanUpService
    {
        private int _numberOfFilesRemoved = 0;
        private EventLog log;

        public FileCleanUpService(EventLog eventLog)
        {
            log = eventLog;
        }

        /// <summary>
        /// This method will delete media files from a configurable directory, 
        /// using a specific search pattern and media type, as defined in VisualCheckConfiguration.
        /// Not the use of Directory.EnumerateFiles() which allows the collection to be enumerated 
        /// before the whole collection is returned. This should help with efficiency.
        /// </summary>
        /// <returns></returns>
        public void DeleteFiles()
        {
            try
            {
                if(!Directory.Exists(VisualCheckConfiguration.Directory))
                    throw new DirectoryNotFoundException();

                foreach (var fileToDelete in Directory.EnumerateFiles(VisualCheckConfiguration.Directory, VisualCheckConfiguration.Pattern))
                {
                    var fileType = Path.GetExtension(fileToDelete);
                    if (!VisualCheckConfiguration.SupportedFileTypes.Contains(fileType))
                        continue;

                    File.Delete(fileToDelete);
                    _numberOfFilesRemoved++;
                }
                log.WriteEntry(String.Format(VisualCheckConfiguration.Success, _numberOfFilesRemoved), EventLogEntryType.Information);            
            }
            catch (Exception ex)
            {
                log.WriteEntry(ex.Message, EventLogEntryType.Error);
            }
            
        }
    }
}
