﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using VisualCheckService.Configuration;
using VisualCheckService.Factories;
using VisualCheckService.Interfaces;

namespace VisualCheckService
{
    public partial class VisualCheckService : ServiceBase
    {
        private CleanUpServiceType _cleanUpServiceType;
        private ICleanUpService _cleanUpService;
        private EventLog _eventLog;

        public VisualCheckService(CleanUpServiceType cleanUpServiceType)
        {
            InitializeComponent();

            _cleanUpServiceType = cleanUpServiceType;

            _eventLog = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists(VisualCheckConfiguration.EventLogSource))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    VisualCheckConfiguration.EventLogSource, VisualCheckConfiguration.EventLog);
            }
            _eventLog.Source = VisualCheckConfiguration.EventLogSource;
            _eventLog.Log = VisualCheckConfiguration.EventLog;
        }

        protected override void OnStart(string[] args)
        {
            _cleanUpService = ServiceFactory.CreateService(_cleanUpServiceType, _eventLog);
            _cleanUpService.DeleteFiles();
        }

        protected override void OnStop()
        {
        }
    }
}
